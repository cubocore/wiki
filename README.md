# Wiki
Information about CuboCore and C Suite.

## Packages

* [Download CSuite v3.0.0 AppImages](AppImages/README.md)

## Website
Check our [website](https://cubocore.gitlab.io).

## Changes in latest release
Check this file for al the changes in the latest release of C Suite. [ReleaseNotes](https://gitlab.com/cubocore/wiki/blob/master/ReleaseNotes).
For more info check [ChangeLog](https://gitlab.com/cubocore/wiki/blob/master/ChangeLog).

## Build C Suite
We have included the way to build Deb, pkg, appimages and build from the sources.
Check out this file for the necessary steps. [BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md)

## Tested In
We have two system to build apps and test them.
Here is the more info. [TestSystem](https://gitlab.com/cubocore/wiki/blob/master/TestSystem)

## Known Issues
Here is the [full list of known bugs](https://gitlab.com/groups/cubocore/coreapps/-/issues)

## Help Us
Help us to make better apps and fix issues by testing them and code contribution.

## Feedback
We need your feedback to improve the C Suite. Send us your feedback through [Gitlab issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).
