# Build and install instructions

 * CoreApps
   * [Build and install from source][coreapps-source]
     * [Example libcprime][coreapps-example-libcprime] 
     * [Uninstall from source][coreapps-uninstall]
   * [Build Arch package][coreapps-arch]
   * [Build Alpine package][coreapps-alpine]
   * [Build Debian package][coreapps-debian]
   * [Build Appimage][coreapps-appimage]
   * [Build Flatpak package][coreapps-flatpak]
   * [Build Snap package][coreapps-snap] (not covered)
   
 * PaperDE
   * [Build and install from source][paper-source]

[coreapps-source]: #build-and-install-coreapps-from-the-source
[coreapps-example-libcprime]: #example-libcprime-using-ninja
[coreapps-uninstall]: #uninstall-from-source
[coreapps-arch]: #build-a-package-for-coreapps-arch-pkgtarxz
[coreapps-alpine]: #build-a-package-for-alpine-apk
[coreapps-debian]: #build-a-package-for-debian-deb
[coreapps-appimage]: #build-an-appimage
[coreapps-flatpak]: #build-a-flatpak-package
[coreapps-snap]: #build-a-snap-package
[paper-source]: #build-and-install-paperde-from-the-source

## Build and install CoreApps from the source

* Make sure you installed all the dependencies needed for an app.
* Download the sources in a folder by `git clone` or download it from GitLab repository. 
* Enter the folder or extract it.
* Now in the terminal type
    ```sh
    cmake -GNinja ./CMakeLists.txt && ninja -k 0 -j $(nproc) 
    ```
    
* Install the build source (as root)
    ```sh
    sudo ninja install
    ```

* Now run the app from terminal or from the start menu.

### Example libcprime (Using ninja)

  ```sh
  git clone https://gitlab.com/cubocore/libcprime.git
  cmake -GNinja ./CMakeLists.txt
  ninja -k 0 -j $(nproc) && sudo ninja install
  ```


### Uninstall from source

You can uninstall if you have the build folder that we created while installing from source.

* Go to the build folder.
* Open terminal and type (as root)

    ```sh
    sudo xargs rm < install_manifest.txt
    ```

## Build a package for CoreApps Arch (*.pkg.tar.xz)
*Note: [Wiki - PKGBUILD](https://gitlab.com/cubocore/wiki/tree/master/PKGBUILD) folder contains all the apps PKGBUILD.*

* Copy or download the `PKGBUILD` file you want to package, to a new folder.
* Open terminal in that folder.
* Type in the terminal
    ```sh
    makepkg -sc PKGBUILD
    ```
* It will download the latest source from git and package it.

## Build a package for Alpine (*.apk)
*Note: [Wiki - APKBUILD](https://gitlab.com/cubocore/wiki/tree/master/APKBUILD) folder contains all the apps APKBUILD.*

* Copy the `APKBUILD` file you want to build, to a new folder.
* Open terminal in that folder
* Type in the terminal 
    ```sh
    abuild -r
    ```
* It will download the latest source from git and package it.


## Build a package for Debian (*.deb)
*Note: [Wiki - DEBUILDS](https://gitlab.com/cubocore/wiki/tree/master/DEBUILDS) folder contains all information needed to make debian packages.*

* Enter the DEBUILDS folder and type in the terminal
    ```sh
    ./build-debpkgs
    ```
* It will build and make the *.deb file for you. You have to manually install them.


## Build an AppImage
*Note: Use the oldest supported Ubuntu 16.04 LTS for making appimage.*

* Go to [linuxdeployqt](https://github.com/probonopd/linuxdeployqt/releases).
* Download `linuxdeployqt-continuous-x86_64.AppImage`
* Copy it in a new folder (e.g. `appimage`).
* Follow [build from source instructions](https://gitlab.com/cubocore/wiki/-/blob/master/BuildInfo.md#build-and-install-from-the-source) but don't install the apps.
* Open terminal in `appimage` folder.
* Type this in terminal 
    ```sh
    ./linuxdeployqt-continuous-x86_64.AppImage <build-folder>
    ./linuxdeployqt-continuous-x86_64.AppImage <build-folder> -appimage
    ```

    *Note: `build-folder` will be the folder name where you build your source.*
* See this [video tutorial](https://www.youtube.com/watch?v=PDzlT_ODpM8&t=694s) for more info.


## Build a Flatpak package
*Note: [Wiki - Flatpak-git](https://gitlab.com/cubocore/wiki/tree/master/Flatpak-git) folder contains all the `*.yml` file to build specific flatpak.*
* Install correct SDK using flatpak install org.kde.Sdk and Platform flatpak install org.kde.Platform
* Copy the `*yml` file you want to build, to a new folder.
* Open terminal in that folder
* Type in the terminal 
    ```sh
    flatpak-builder --user --install build-dir --force-clean ./cc.cubocore.<appsname>
    flatpak run cc.cubocore.<appsname> # Run the app
    ```
    
    *Note: `<appsname>` is name of the apps. Ex. CorePad*
    
* It will download the latest source from git, compile it and install.


## Build a Snap package.
***We don't know how. If you know let us know the steps and help us to reach.***


<br/>
<br/>


## Build and install PaperDE from the source

***Please be up to date from the dependency list [here](https://gitlab.com/cubocore/paper/paperde#dependencies).***


## Build and install [paperde](https://gitlab.com/cubocore/paper/paperde)

```sh
# Download the git version of paperde
git clone https://gitlab.com/cubocore/paper/paperde paperde
cd paperde && mkdir build && cd build
meson .build --prefix=/usr --buildtype=release
ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install
```


***Feel free to add instructions for your specific distro, tips and trick you have used to make Paper/Wayfire an enjoyable experience.***
